#!/bin/sh

# Primitive testing script to check that the JSONs are valid.

ret=0
while read -r l
do
	if ! jq --exit-status '.route | .[] | .description' < "$l" > /dev/null 2>&1
	then
		echo "$l is invalid." >&2
		ret=1
	else
		#echo "$l is valid."
		:
	fi
done < <(find "$PWD" -name '*.json' -type f)

exit $ret
